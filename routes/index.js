const express = require('express');
const router = express.Router();
const amount = require('../controllers/amounts');

router.get('/', function(req, res){
    amount.index(req,res);
});

router.post('/addamount', function(req, res) {
    amount.create(req,res);
});

router.get('/addamount', function(req, res) {
    amount.list(req,res);
});

module.exports = router;
