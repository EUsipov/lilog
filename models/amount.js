const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Amount = new Schema ({
        date: { type: Date, default: Date.now },
        dosage: { type: String, required: true },
});

module.exports = mongoose.model('Amount', Amount)