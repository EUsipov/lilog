const path = require('path');
const Amount = require('../models/amount');

exports.index = function (req, res) {
    //res.sendFile(path.resolve('views/sharks.html'));
    Amount.find({}).exec(function (err, amounts) {
        if (err) {
                return res.send(500, err);
        }
        res.render('index', {
                amounts: amounts
     });
});
};

exports.create = function (req, res) {
    var newAmount = new Amount(req.body);
    console.log(req.body);
    newAmount.save(function (err) {
        if(err) {
            res.status(400).send('Unable to save amount to database');
        } else {
            res.redirect('/');
        }
  });
};

exports.list = function (req, res) {
        Amount.find({}).exec(function (err, amounts) {
                if (err) {
                        return res.send(500, err);
                }
                res.render('addamount', {
                        amounts: amounts
             });
        });
};
